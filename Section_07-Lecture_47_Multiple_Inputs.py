import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import  Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd

# create any data for the dashboard
df = pd.read_csv('data/mpg.csv')
features = df.columns

# Create the application instance:
app = dash.Dash()

# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    html.Div([
        dcc.Dropdown(id='xaxis',
                     options=[{'label': i, 'value': i}
                              for i in features],
                     value='displacement')
    ], style={'width': '48%', 'display': 'inline-block'}),

    html.Div([
        dcc.Dropdown(id='yaxis',
                     options=[{'label': i, 'value': i}
                              for i in features],
                     value='weight')
    ], style={'width': '48%', 'display': 'inline-block'}),

    dcc.Graph(id='feature_graphic')
], style={'padding': 10})


# Create any callbacks needed
@app.callback(Output(component_id='feature_graphic', component_property='figure'),
              [Input(component_id='xaxis', component_property='value'),
               Input(component_id='yaxis', component_property='value')])
def update_graph(xaxis_name, yaxis_name):

    return {'data': [go.Scatter(x=df[xaxis_name],
                                y=df[yaxis_name],
                                text=df['name'],
                                mode='markers',
                                marker={'size': 15,
                                        'opacity': 0.4,
                                        'line': {'width': 0.5, 'color': 'white'}})],
            'layout': go.Layout(title='Car Plot Dashboard',
                                xaxis={'title': xaxis_name},
                                yaxis={'title': yaxis_name},
                                hovermode='closest')
            }


# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)


















# Add the server clause:
