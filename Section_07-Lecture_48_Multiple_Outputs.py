import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import  Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import base64


def encode_image(image_file):
    encoded = base64.b64encode(open(image_file,'rb').read())
    return 'data:image/png;base64,{}'.format(encoded.decode())


# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Create any data for the dashboard
df = pd.read_csv('data/wheels.csv')

# Create the application instance:
app = dash.Dash()

# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    dcc.RadioItems(id='wheels',
                   options=[{'label': i, 'value': i} for i in df['wheels'].unique()],
                   value=1),
    html.Div(id='wheels_output'),
    html.Hr(),
    dcc.RadioItems(id='colors',
                   options=[{'label': i, 'value': i} for i in df['color'].unique()],
                   value='blue'),
    html.Div(id='colors_output'),
    html.Div(id='picpath_output'),
    html.Img(id='display_image', src='children', height=300)
], style={'fontfamily': 'helvetica', 'fontsize': 18})


# Create any callbacks needed
@app.callback(Output('wheels_output', 'children'),
              [Input('wheels', 'value')])
def callback_a(wheels_value):
    return "you entered: {}".format(wheels_value)


@app.callback(Output('colors_output', 'children'),
              [Input('colors', 'value')])
def callback_a(colors_value):
    return "you entered: {}".format(colors_value)


@app.callback(Output('display_image', 'src'),
              [Input('wheels', 'value'),
               Input('colors', 'value')])
def callback_image(wheel, color):
    path = 'data/images/'
    return encode_image(path+df[(df['wheels'] == wheel) &
                                (df['color'] == color)]['image'].values[0])


@app.callback(Output('picpath_output', 'children'),
              [Input('wheels', 'value'),
               Input('colors', 'value')])
def callback_image(wheel, color):
    path = 'data/images/'
    return "{}".format(path+df[(df['wheels'] == wheel) &
                               (df['color'] == color)]['image'].values[0])


# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)
