import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy as np
import pandas as pd

# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Create the application instance:
app = dash.Dash()

# create any data for the dashboard
# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    dcc.Graph(
        id='',
        figure={'data': [],'layout': go.Layout()}
    )
])

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)


















# Add the server clause:
