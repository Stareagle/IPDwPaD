#######
# Here we'll make a scatter plot with fake data that is
# intentionally denser on the left, with overlapping data points.
# We'll use Selection Data to uncover the difference.
######
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import socket

# Create the Dash App instance
app = dash.Dash()

# create x and y arrays
np.random.seed(10)                # for reproducible results
x_1 = np.linspace(0.1, 5, 50)     # left half
x_2 = np.linspace(5.1, 10, 50)    # right half
y = np.random.randint(0, 50, 50)  # 50 random points

# create three "half DataFrames"
df_0 = pd.DataFrame({'x': x_1, 'y': y})
df_1 = pd.DataFrame({'x': x_1, 'y': y})
df_2 = pd.DataFrame({'x': x_2, 'y': y})

# combine them into one DataFrame (df1 and df2 points overlap!)
df = pd.concat([df_0, df_1, df_2])

# Create the app layout
app.layout = html.Div([
    html.Div([
        dcc.Graph(
            id='plot',
            figure={
                'data': [go.Scatter(
                    x=df['x'],
                    y=df['y'],
                    mode='markers'
                )],
                'layout': go.Layout(
                    title='Density',
                    hovermode='closest'
                )
            }
        )

    ], style={'width': '40%', 'display':'inline-block'}),
    html.Div([
        html.H1(
            id='density',
            style={'paddingTop': 25}
        )
    ], style={'width': "30%", 'display': 'inline-block', 'verticalAlign': 'top'})
])

# Create any callbacks and their functions that need to be used
@app.callback(
    Output('density','children'),
    [Input('plot', 'selectedData')])
def get_density(selected_data):
    # Calculate the density
    pts = len(selected_data['points'])
    rng_or_lp = list(selected_data.keys())
    rng_or_lp.remove('points')
    max_x = max(selected_data[rng_or_lp[0]]['x'])
    min_x = min(selected_data[rng_or_lp[0]]['x'])
    max_y = max(selected_data[rng_or_lp[0]]['y'])
    min_y = min(selected_data[rng_or_lp[0]]['y'])
    area = (max_x-min_x) * (max_y-min_y)
    d = pts / area
    return 'Density = {:.3f}'.format(d)


# get the current IP address of the host
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)