#######
# This provides examples of Dash Core Components.
# Feel free to add things to it that you find useful.
######
import socket
import dash
import dash_core_components as dcc
import dash_html_components as html

# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Create the application instance:
app = dash.Dash()

# create any data for the dashboard
# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    # DROPDOWN https://dash.plot.ly/dash-core-components/dropdown
    html.Label('Dropdown'),
    dcc.Dropdown(options=[
        {'label': 'New York City', 'value': 'NYC'},
        {'label': 'San Francisco', 'value': 'SF'}],
        value='SF'),

    html.Label('Slider'),
    dcc.Slider(
        min=-10,
        max=10,
        step=0.5,
        value=0,
        marks={i: i for i in range(-10, 10)}),

    html.P(html.Label('Some Radio Items')),
    dcc.RadioItems(options=[{'label': 'New York City',
                             'value': 'NYC'},
                            {'label': 'San Francisco',
                             'value': 'SF'}
                            ],
                   value='SF')
])

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)


















# Add the server clause:
