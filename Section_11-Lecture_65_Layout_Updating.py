# Module imports
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import socket

# Create the Dash App instance
app = dash.Dash()


# Create any data the will be used in the app

# Create the app layout
app.layout = html.Div([
    html.H1(id='live-update-text'),
    dcc.Interval(
        id='interval-component',
        interval=2000,
        n_intervals=0
    )
])


# Create any callbacks and their functions that need to be used
@app.callback(
    Output('live-update-text', 'children'),
    [Input('interval-component','n_intervals')])
def update_layout(n):
    return 'Crash free for {} refreshes'.format(n)


# get the current IP address of the host
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)
