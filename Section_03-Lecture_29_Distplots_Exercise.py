#######
# Objective: Using the iris dataset, develop a Distplot
# that compares the petal lengths of each class.
# File: '../data/iris.csv'
# Fields: 'sepal_length','sepal_width','petal_length','petal_width','class'
# Classes: 'Iris-setosa','Iris-versicolor','Iris-virginica'
######

# Perform imports here:
import plotly.offline as pyo
import plotly.figure_factory as ff
import pandas as pd

# create a DataFrame from the .csv file:
df = pd.read_csv('data/iris.csv')
# Define the traces

# HINT:
# This grabs the petal_length column for a particular flower
a = df[df['class'] == 'Iris-setosa']['petal_length']
b = df[df['class'] == 'Iris-versicolor']['petal_length']
c = df[df['class'] == 'Iris-virginica']['petal_length']

histdata = [a, b, c]
grouplabels = ['Setosa', 'Versicolor', 'Virginica']
fig = ff.create_distplot(histdata, grouplabels)
pyo.plot(fig)

# Define a data variable



# Create a fig from data and layout, and plot the fig
