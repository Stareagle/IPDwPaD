# Module imports
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import json
import socket

# Create the Dash App instance
app = dash.Dash()

# Create any data the will be used in the app

# Create the app layout
app.layout = html.Div([
    html.H1('Stock Ticker Dashboard'),
    html.H3('Enter a stock symbol'),
    dcc.Input(
        id='stock_picker',
        value='TSLA'
    ),
    dcc.Graph(
        id='my_graph',
        figure={'data': [
            {'x': [1, 2], 'y': [3, 1]}
        ], 'layout': {'title': 'Default Title'}}
    )
])


# Create any callbacks and there functions
@app.callback(Output('my_graph', 'figure'),
              [Input('stock_picker', 'value')])
def update_graph(stock_ticker):
    fig = {'data': [{'x': [1, 5], 'y': [3, 1]}],
           'layout': {'title': stock_ticker}
           }
    return fig

# get the current IP address of the host
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)