#######
# First Milestone Project: Develop a Stock Ticker
# dashboard that either allows the user to enter
# a ticker symbol into an input box, or to select
# item(s) from a dropdown list, and uses pandas_datareader
# to look up and display stock data on a graph.
######

# ADD A DATEPICKERRANGE COMPONENT FOR START & END DATE INPUT
# Module imports
import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas_datareader.data as web # requires v0.6.0 or later
import pandas as pd
from datetime import datetime
from dash.dependencies import Input, Output, State
app = dash.Dash()

# Create any data the will be used in the app
nsdq = pd.read_csv('data/NASDAQcompanylist.csv')
nsdq.set_index('Symbol', inplace=True)
options = []

for tic in nsdq.index:
    mydict = {}
    mydict['label'] = nsdq.loc[tic]['Name'] + ' ' + tic
    mydict['value'] = tic
    options.append(mydict)

# Create the app layout
app.layout = html.Div([
    html.H1('Stock Ticker Dashboard'),
    html.Div([
        html.H3('Enter a stock symbol:',
                style={'paddingRight':'30px'}),
        dcc.Dropdown(
            id='stock_picker',
            options=options,
            value=['TSLA'],
            multi=True),
    ], style={'display': 'inline-block', 'verticalAlign': 'top', 'width':'30%'}),
    html.Div([
        html.H3('Select start and end dates:'),
        dcc.DatePickerRange(
            id='date-picker',
            min_date_allowed = datetime(2015, 1, 1),
            max_date_allowed = datetime.today(),
            start_date = datetime(2018, 1, 1),
            end_date = datetime.today()
        )
    ], style={'display': 'inline-block'}),
    html.Div([html.Button(
        id='submit_button_clicked',
        n_clicks=0,
        children='submit',
        style={'fontsize': 35, 'marginLeft': '30px'})
    ], style={'display': 'inline-block'}),
    dcc.Graph(
        id='my_graph',
        figure={'data': [
            {'x': [1, 2], 'y': [3, 1]}
        ], 'layout': {'title': 'Default Title'}}
    )
])


# Create any callbacks and there functions
@app.callback(
    Output('my_graph', 'figure'),
    [Input('submit_button_clicked', 'n_clicks')],
    [State('stock_picker', 'value'),
     State('date-picker', 'start_date'),
     State('date-picker', 'end_date')
     ])
def update_graph(n_clicks, stock_ticker, start_date, end_date):
    start = datetime.strptime(start_date[:10], '%Y-%m-%d')
    end = datetime.strptime(end_date[:10], '%Y-%m-%d')
    traces = []
    for tic in stock_ticker:
        df = web.DataReader(tic,'iex',start,end)
        traces.append({'x': df.index, 'y': df['close'], 'name': tic})
    fig = {
        'data': traces,
        'layout': {'title':stock_ticker}
    }
    return fig

# get the current IP address of the host
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)