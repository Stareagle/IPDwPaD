# Module imports
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import json
import socket

# Create the Dash App instance
app = dash.Dash()

# Create any data the will be used in the app
df = pd.read_csv('data/mpg.csv')

# Create the app layout
app.layout = html.Div([
    dcc.Graph(
        id='mpg-scatter',
        figure={
            'data': [go.Scatter(
                x=df['model_year']+1900,
                y=df['mpg'],
                text=df['name'],
                hoverinfo='text'+'y'+'x',
                mode='markers'
            )],
            'layout': go.Layout(title='Mpg Data',
                         xaxis={'title': 'Model Year'},
                         yaxis={'title': 'MPG'},
                         hovermode='closest')
        },
    )
])

# get the current IP address of the host
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)