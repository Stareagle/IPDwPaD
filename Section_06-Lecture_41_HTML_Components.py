import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy as np
import pandas as pd

# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Create the application instance:
app = dash.Dash()


app.layout = html.Div(
    [
        'This is the outermost div!',
        html.Div(['This is an inner Div!'],
                 style={
                     'color': 'red',
                     'border': '2px red solid'
                 }),
        html.Div(['Another inner div!'],
                 style={
                     'color': 'blue',
                     'border': '3px blue solid'
                 })
    ],
    style={
        'color': 'green',
        'border': '2px green solid'
    }
)

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)
