import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import  Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd

# Create the application instance:
app = dash.Dash()

# create any data for the dashboard
# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    dcc.Input(id='my_id', value='Initial Text', type='text'),
    html.Div(id='my_div')
])


# Create any callbacks needed
@app.callback(Output(component_id='my_div', component_property='children'),
              [Input(component_id='my_id', component_property='value')])
def update_output_dev(input_value):
    return "Tou entered: {}".format(input_value)


# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)


















# Add the server clause:
