#######
# Objective: Create a bubble chart that compares three other features
# from the mpg.csv dataset. Fields include: 'mpg', 'cylinders', 'displacement'
# 'horsepower', 'weight', 'acceleration', 'model_year', 'origin', 'name'
######

# Perform imports here:
import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd

# create a DataFrame from the .csv file:
df = pd.read_csv('data/mpg.csv')

# create data by choosing fields for x, y and marker size attributes
data = [go.Scatter(x=df['horsepower'],
                   y=df['acceleration'],
                   text=df['name'],
                   mode='markers',
                   marker=dict(
                       size=df['model_year']/5,
                       color=df['mpg'],
                       showscale=True
                   ))]

# create a layout with a title and axis labels
layout = go.Layout(title='Car Data',
                   hovermode='closest')

# create a fig from data & layout, and plot the fig
fig = go.Figure(data, layout)
pyo.plot(fig)
