#######
# Objective: Create a stacked bar chart from
# the file ../data/mocksurvey.csv. Note that questions appear in
# the index (and should be used for the x-axis), while responses
# appear as column labels.  Extra Credit: make a horizontal bar chart!
######

# Perform imports here:
import plotly.offline as pyo
import plotly.graph_objs as go
import pandas as pd


# create traces using a list comprehension:
def plot_graph(data):
    layout = go.Layout(title='Survey', barmode='stack')
    fig = go.Figure(data, layout)
    pyo.plot(fig)


# create a DataFrame from the .csv file:
df = pd.read_csv('data/mocksurvey.csv', index_col=0)
plot_graph([go.Bar(x=df.index, y=df[response], name=response)
            for response in df.columns])

plot_graph([go.Bar(y=df.index, x=df[response], name=response, orientation='h')
            for response in df.columns])








# create a layout, remember to set the barmode here





# create a fig from data & layout, and plot the fig.
