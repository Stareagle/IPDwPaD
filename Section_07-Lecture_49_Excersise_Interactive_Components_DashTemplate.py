import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import  Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd

# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Create the application instance:
app = dash.Dash()

# create any data for the dashboard
# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    html.P(
        dcc.RangeSlider(
            id='multiply_slider',
            marks={i: '{}'.format(i) for i in range(-10, 11)},
            min=-10,
            max=10,
            value=[-5, 5],
            allowCross=False
        )
    ),
    html.Div(html.H1(id='multiply_slider_output'))
])


# Create any callbacks needed
@app.callback(Output('multiply_slider_output', 'children'),
              [Input('multiply_slider', 'value')])
def update_output_dev(input_value):
    return "{} multiplied by {} = {}".format(input_value[0], input_value[1], input_value[0]*input_value[1])


# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)

#######
# Objective: Create a dashboard that takes in two or more
# input values and returns their product as the output.
######
# Perform imports here:
# Launch the application:
# Create a Dash layout that contains input components
# and at least one output. Assign IDs to each component:
# Create a Dash callback:
# Add the server clause:









