 # Module imports
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import json
import socket

# Create the Dash App instance
app = dash.Dash()

# Create any data the will be used in the app
df = pd.read_csv('data/mpg.csv')

# Create the app layout
app.layout = html.Div([
    html.Div([
        dcc.Graph(
            id='mpg-scatter',
            figure={
                'data': [go.Scatter(
                    x=df['model_year'] + 1900,
                    y=df['mpg'],
                    text=df['name'],
                    hoverinfo='text' + 'y' + 'x',
                    mode='markers'
                )],
                'layout': go.Layout(title='Mpg Data',
                                    xaxis={'title': 'Model Year'},
                                    yaxis={'title': 'MPG'},
                                    hovermode='closest')
            },
        )
    ], style={'width=': '50%', 'display': 'inline-block'}),
    html.Div([
        dcc.Graph(
            id='mpg_acceleration',
            figure={
                'data': [
                    go.Scatter(x=[0, 1],
                               y=[0, 1],
                               mode='lines')
                ],
                'layout': go.Layout(title='Acceleration',
                                    xaxis={'visible': False},
                                    yaxis={'visible': False},
                                    margin={'l': 0})
            }
        )
    ], style={'width': '40%', 'display':'inline-block'}),
    html.Div([
        dcc.Markdown(id='mpg_acceleration_stats'),
    ], style={'width': '20%', 'height': '50%', 'display': 'inline-block'})
])


# Create any callbacks and their functions that need to be used
@app.callback(
    Output('mpg_acceleration', 'figure'),
    [Input('mpg-scatter', 'hoverData')])
def callback_graph(hover_data):
    v_index = hover_data['points'][0]['pointIndex']
    figure = {'data': [go.Scatter(
        x=[0,1],
        y=[0, 60/df.iloc[v_index]['acceleration']],
        mode='lines',
        line={'width': 2 * df.iloc[v_index]['cylinders']}
    )],
              'layout': go.Layout(title=df.iloc[v_index]['name'],
                                  xaxis={'visible': False},
                                  yaxis={'visible': False,'range': [0,60/df['acceleration'].min()]},
                                  margin={'l': 0},
                                  height=300)
              }
    return figure

@app.callback(
    Output('mpg_acceleration_stats', 'children'),
    [Input('mpg-scatter', 'hoverData')])
def callback_stats(hover_data):
    v_index = hover_data['points'][0]['pointIndex']
    stats = """
        {} cylinders
            {}cc dislacement
            0 to 60mph in {} seconds
    """.format(df.iloc[v_index]['cylinders'],
               df.iloc[v_index]['displacement'],
               df.iloc[v_index]['acceleration'])
    return stats


# get the current IP address of the host
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)