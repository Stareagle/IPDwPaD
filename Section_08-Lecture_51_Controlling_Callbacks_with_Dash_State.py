import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Create the application instance:
app = dash.Dash()

# create any data for the dashboard

# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    dcc.Input(id='number_in',
              value=1,
              style={'fontSize': 24}),
    html.Button(id='btn_submit',
                n_clicks=0,
                children='Submit',
                style={'fontSize': 24}),
    html.H1(id='number_out')
])


# Create any callbacks needed
@app.callback(Output('number_out', 'children'),
              [Input('btn_submit', 'n_clicks')],
              [State('number_in', 'value')])
def update_output(n_clicks, input_value):
    return 'Data entered was {} and you hace clicked the button {} times'.format(input_value, n_clicks)


# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)

#######
# Objective: Create a dashboard that takes in two or more
# input values and returns their product as the output.
######

# Perform imports here:
# Launch the application:
# Create a Dash layout that contains input components
# and at least one output. Assign IDs to each component:
# Create a Dash callback:
# Add the server clause:
















# Add the server clause:
