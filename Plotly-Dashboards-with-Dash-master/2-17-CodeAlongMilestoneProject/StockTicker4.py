#######
# First Milestone Project: Develop a Stock Ticker
# dashboard that either allows the user to enter
# a ticker symbol into an input box, or to select
# item(s) from a dropdown list, and uses pandas_datareader
# to look up and display stock data on a graph.
######

# ADD A DATEPICKERRANGE COMPONENT FOR START & END DATE INPUT
# Module imports
import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas_datareader.data as web # requires v0.6.0 or later
from datetime import datetime
from dash.dependencies import Input, Output
app = dash.Dash()

# Create any data the will be used in the app

# Create the app layout
app.layout = html.Div([
    html.H1('Stock Ticker Dashboard'),
    html.Div([
        html.H3('Enter a stock symbol:',
                style={'paddingRight':'30px'}),
        dcc.Input(
            id='stock_picker',
            value='TSLA',
            style={'fontSize': 24, 'width': 75})
    ], style={'display': 'inline-block', 'verticalAlign': 'top'}),
    html.Div([
        html.H3('Select start and end dates:'),
        dcc.DatePickerRange(
            id='my_date_picker',
            min_date_allowed = datetime(2015, 1, 1),
            max_date_allowed = datetime.today(),
            start_date = datetime(2018, 1, 1),
            end_date = datetime.today()
        )
    ], style={'display': 'inline-block'}),
    dcc.Graph(
        id='my_graph',
        figure={'data': [
            {'x': [1, 2], 'y': [3, 1]}
        ], 'layout': {'title': 'Default Title'}}
    )
])


# Create any callbacks and there functions
@app.callback(
    Output('my_graph', 'figure'),
    [Input('stock_picker', 'value'),
     Input('date-picker', 'start_date'),
     Input('date-picker', 'end_date')
     ])
def update_graph(stock_ticker, start_date, end_date):
    start = datetime.strptime(start_date[:10], '%Y-%m-%d')
    end = datetime.strptime(end_date[:10], '%Y-%m-%d')
    df = web.DataReader(stock_ticker,'iex',start,end)
    fig = {
        'data': [
            {'x': df.index, 'y': df.close}
        ],
        'layout': {'title':stock_ticker}
    }
    return fig

if __name__ == '__main__':
    app.run_server()
