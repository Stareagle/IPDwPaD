# https://dash.plot.ly/dash-core-components/dropdown

import socket
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import pandas as pd

# create any data for the dashboard
df = pd.read_csv('data/gapminderDataFiveYear.csv')

# We need to construct a dictionary of dropdown values for the years
year_options = []
for year in df['year'].unique():
    year_options.append({'label': str(year), 'value': year})

# Create the application instance:
app = dash.Dash()

# Create a Dash layout that contains a Graph component:
app.layout = html.Div([
    dcc.Graph(id='graph'),
    dcc.Dropdown(id='year_picker', options=year_options, value=df['year'].min())
])


# Create any callbacks needed
@app.callback(Output('graph', 'figure'),
              [Input('year_picker', 'value')])
def update_figure(selected_year):
    # Data only for selected year from dropdown
    filtered_df = df[df['year'] == selected_year]

    traces = []
    for continent_name in filtered_df['continent'].unique():
        df_by_continent = filtered_df[filtered_df['continent'] == continent_name]
        traces.append(go.Scatter(
            x=df_by_continent['gdpPercap'],
            y=df_by_continent['lifeExp'],
            text=df_by_continent['country'],
            mode='markers',
            opacity=0.7,
            marker={'size': 15},
            name=continent_name
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'type': 'log', 'title': 'GDP Per Capita'},
            yaxis={'title': 'Life Expectancy'},
            hovermode='closest'
        )
    }


# get the host ip
host_ip = socket.gethostbyname(socket.gethostname())

# Run the application server
if __name__ == '__main__':
    app.run_server(debug=True, host=host_ip)
